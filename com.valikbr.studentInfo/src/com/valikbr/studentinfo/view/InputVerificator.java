package com.valikbr.studentinfo.view;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.widgets.Text;

public class InputVerificator {

	public static boolean verifyName(String s) {

		String regex = "(?i)(^[a-z])((?![ .,'-]$)[a-z .,'-]){1,24}$";
		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(s);
		boolean b = matcher.matches();
		return b;
	}

	public static boolean verifyAddress(String s) {

		String regex = "^[#.0-9a-zA-Z\\s,-]+$";
		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(s);
		boolean b = matcher.matches();
		return b;
	}

	public static boolean verifyCity(String s) {

		String regex = "^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";
		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(s);
		boolean b = matcher.matches();
		return b;
	}

	public static void verifyGroup(VerifyEvent event) {

		String newText = event.text;
		String str = ((CCombo) event.getSource()).getText();
		str = str + newText;

		String pattern = "^[1-9][0-9]*$";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);

		if (!m.matches()) {
			event.doit = false;
			return;
		}
	}

	public static void verifyResult(VerifyEvent event) {

		String newText = event.text;
		String str = ((Text) event.getSource()).getText();
		str = str + newText;

		String pattern = "^[1-9][0-9]*$";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);

		if (!m.matches()) {
			event.doit = false;
			return;
		}
	}

}
