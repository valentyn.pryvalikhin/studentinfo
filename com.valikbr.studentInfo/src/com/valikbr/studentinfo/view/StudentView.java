package com.valikbr.studentinfo.view;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;

import com.valikbr.studentinfo.entities.Student;
import com.valikbr.studentinfo.model.GroupModel;

public class StudentView {

	StudentTabFolder tabFolder;

	@Inject
	MApplication appl;

	public static final String TABFOLDER = "tabfolder";

	@Inject
	@Optional
	private void listenToStudent(@UIEventTopic(GroupView.GROUPVIEW_STUDENT) Student student) {
		tabFolder.openStudentTab(student);
	}

	@PostConstruct
	public void postConstruct(Composite parent) {

		tabFolder = new StudentTabFolder(parent, SWT.NONE, appl);
		

		DropTarget target = new DropTarget(tabFolder, DND.DROP_COPY);

		TextTransfer textTransfer = TextTransfer.getInstance();
		Transfer[] types = new Transfer[] {textTransfer};
		target.setTransfer(types);

		target.addDropListener(new DropTargetAdapter() {

			@Override
			public void drop(DropTargetEvent event) {
				if (textTransfer.isSupportedType(event.currentDataType)) {
					String text = (String) event.data;
					GroupModel model = (GroupModel) appl.getContext().get(GroupView.MODEL);
					Student student = model.getStudent(Integer.parseInt(text));
					tabFolder.openStudentTab(student);
				}
			}
			
			@Override
			public void dragEnter(DropTargetEvent event) {
					 event.detail = DND.DROP_COPY;
				 }
			
		});

		appl.getContext().set(TABFOLDER, tabFolder);
	}
}
