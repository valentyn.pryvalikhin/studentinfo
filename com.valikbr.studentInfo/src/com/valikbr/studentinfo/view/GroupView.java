package com.valikbr.studentinfo.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;

import com.valikbr.studentinfo.IOOperations;
import com.valikbr.studentinfo.entities.Student;
import com.valikbr.studentinfo.model.GroupModel;
import com.valikbr.studentinfo.model.GroupModelImpl;

public class GroupView {

	public static final String GROUPVIEW_STUDENT = "groupView/student";
	public static final String MODEL = "model";
	public static final String SELECTED_STUDENT = "selectedStudent";
	public static final String TREE_VIEW = "treeView";
	public static final String POPUP_MENU = "com.valikbr.studentinfo.popupmenu.popupMenu";

	@Inject
	IEventBroker ebroker;

	@Inject
	MApplication appl;

	@PostConstruct
	public void postConstruct(Composite parent, EMenuService menu) {
		TreeViewer viewer = new TreeViewer(parent);
		viewer.setContentProvider(new TreeContentProvider());
		viewer.getTree().setHeaderVisible(true);
		viewer.setLabelProvider(new TreeLabelProvider());
		appl.getContext().set(TREE_VIEW, viewer);

		viewer.addDoubleClickListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();

			if (selection.getFirstElement() instanceof Student) {
				Student student = (Student) selection.getFirstElement();
				ebroker.send(GROUPVIEW_STUDENT, student);
			}
		});

		viewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			if (selection.getFirstElement() instanceof Student) {
				appl.getContext().set(SELECTED_STUDENT, selection.getFirstElement());
			} else {
				appl.getContext().set(SELECTED_STUDENT, null);
			}
		});

		menu.registerContextMenu(viewer.getTree(), POPUP_MENU);

		List<GroupModel> groupModels = new ArrayList<>();
		GroupModel model = new GroupModelImpl();
		appl.getContext().set(MODEL, model);
		groupModels.add(model);
		viewer.setInput(groupModels);

		GridLayoutFactory.fillDefaults().generateLayout(parent);

		DragSource source = new DragSource(viewer.getTree(), DND.DROP_COPY);
		Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
		source.setTransfer(types);

		source.addDragListener(new DragSourceAdapter() {

			@Override
			public void dragStart(DragSourceEvent event) {

				event.doit = (viewer.getStructuredSelection().getFirstElement() instanceof Student) ? true : false;
			}

			@Override
			public void dragSetData(DragSourceEvent event) {
				
				IStructuredSelection selection = viewer.getStructuredSelection();
				Student firstElement = (Student) selection.getFirstElement();

				if (TextTransfer.getInstance().isSupportedType(event.dataType)) {
					event.data = String.valueOf(firstElement.getId());
				}
			}
		});
	}
}
