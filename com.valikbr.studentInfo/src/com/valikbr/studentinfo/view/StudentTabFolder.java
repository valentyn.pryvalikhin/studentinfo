package com.valikbr.studentinfo.view;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;

import com.valikbr.studentinfo.entities.Student;
import com.valikbr.studentinfo.model.GroupModel;

public class StudentTabFolder extends CTabFolder {

	public static final String SAVE_STUDENT = "studentTabFolder/saveStudent";

	private Map<Integer, Student> openedStudents = new HashMap();

	private MApplication appl;

	public StudentTabFolder(Composite parent, int style, MApplication appl) {
		super(parent, style);
		this.appl = appl;
	}

	public void createNewStdTab() {

		Composite tabComposite = new Composite(this, SWT.NONE);
		StudentTabItem tabItem = new StudentTabItem(this, SWT.CLOSE, tabComposite, null, appl);
		this.setSelection(tabItem);
	}

	public void openStudentTab(Student student) {

		if (!openedStudents.containsValue(student)) {
			Composite tabComposite = new Composite(this, SWT.NONE);
			StudentTabItem tabItem = new StudentTabItem(this, SWT.CLOSE, tabComposite, student, appl);

			this.setSelection(tabItem);
			openedStudents.put(tabItem.hashCode(), student);

			tabItem.addDisposeListener(e -> {
				openedStudents.remove(tabItem.hashCode());
			});
		}
	}

	public void setComboGroupsItems() {

		String[] comboList = Arrays
				.asList(((GroupModel) appl.getContext().get(GroupView.MODEL)).getGroups().keySet().toArray())
				.toArray(new String[0]);

		CTabItem[] items = this.getItems();
		for (CTabItem it : items) {
			StudentTabItem item = (StudentTabItem) it;
			item.getComboGroup().setItems(comboList);
			if (item.getStudent() != null) {
				item.getComboGroup().setText(item.getStudent().getGroup().getGroupName());
			}
		}
	}

	public void closeTab(Student student) {
		Integer tabHash = getKeyByValue(openedStudents, student);
		CTabItem[] items = this.getItems();
		CTabItem item = null;

		if (tabHash != null) {

			for (CTabItem it : items) {
				if (it.hashCode() == tabHash)
					item = it;
			}
			if (item != null)
				item.dispose();
		}
	}

	private Integer getKeyByValue(Map<Integer, Student> map, Student value) {
		for (Entry<Integer, Student> entry : map.entrySet()) {
			if (Objects.equals(value, entry.getValue())) {
				return entry.getKey();
			}
		}
		return null;
	}
}
