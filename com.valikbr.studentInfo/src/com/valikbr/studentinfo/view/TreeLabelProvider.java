package com.valikbr.studentinfo.view;

import org.eclipse.jface.viewers.LabelProvider;

import com.valikbr.studentinfo.entities.Group;
import com.valikbr.studentinfo.entities.Student;
import com.valikbr.studentinfo.model.GroupModelImpl;

public class TreeLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof GroupModelImpl) {
			return "Folder";
		} else {
			if (element instanceof Group) {
				return "Group " + ((Group) element).getGroupName();
			} else {
				if (element instanceof Student) {
					return ((Student) element).getName();
				} else {
					return super.getText(element);
				}
			}
		}
	}

}
