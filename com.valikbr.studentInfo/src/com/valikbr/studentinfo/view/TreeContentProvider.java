package com.valikbr.studentinfo.view;

import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.valikbr.studentinfo.entities.Group;
import com.valikbr.studentinfo.entities.Student;
import com.valikbr.studentinfo.model.GroupModel;
import com.valikbr.studentinfo.model.GroupModelImpl;

public class TreeContentProvider implements ITreeContentProvider {

	private static final Object[] EMPTY_ARRAY = new Object[0];
	GroupModel groupModel = new GroupModelImpl();
	Map<String, Group> groups = groupModel.getGroups();

	@Override
	public Object[] getChildren(Object parent) {
		if (parent instanceof List) {
			List<GroupModel> groupModel = (List<GroupModel>) parent;
			return groupModel.toArray();
		} else {
			if (parent instanceof GroupModel) {
				GroupModel groupModel = (GroupModel) parent;
				Map<String, Group> groups = groupModel.getGroups();
				return groups.values().toArray();
			} else {
				if (parent instanceof Group) {
					Group group = (Group) parent;
					return group.getStudents().values().toArray();
				}
			}
		}
		return EMPTY_ARRAY;
	}

	@Override
	public Object[] getElements(Object inputElement) {

		if (inputElement instanceof List) {
			List<GroupModel> groupModel = (List<GroupModel>) inputElement;
			return groupModel.toArray();
		} else {
			return EMPTY_ARRAY;
		}
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof Group)
			return groupModel;
		else {
			if (element instanceof Student)
				return ((Student) element).getGroup();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof List || (element instanceof GroupModel && isGroupModelNotEmpty((GroupModel) element))
				|| (element instanceof Group) && isGroupNotEmpty((Group) element))
			return true;
		else
			return false;
	}

	private boolean isGroupModelNotEmpty(GroupModel groupModel) {
		if (groupModel.getGroups().isEmpty())
			return false;
		else {
			return true;
		}
	}

	private boolean isGroupNotEmpty(Group group) {
		if (group.getStudents().isEmpty())
			return false;
		else {
			return true;
		}
	}
}
