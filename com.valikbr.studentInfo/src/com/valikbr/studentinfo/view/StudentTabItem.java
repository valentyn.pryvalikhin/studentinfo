package com.valikbr.studentinfo.view;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.valikbr.studentinfo.entities.Student;
import com.valikbr.studentinfo.model.GroupModel;
import com.valikbr.studentinfo.model.GroupModelImpl;

public class StudentTabItem extends CTabItem {

	public static final String ACCEPT_ICON = "platform:/plugin/com.valikbr.studentInfo/icons/checked.png";
	public static final String CANCEL_ICON = "platform:/plugin/com.valikbr.studentInfo/icons/close.png";
	public static final String DEFAULT_PHOTO = "platform:/plugin/com.valikbr.studentInfo/icons/gomernew.png";

	public static final int PERCENTS_FROM_TOP = 50;
	public static final int PERCENTS_FROM_LEFT = 50;
	public static final int SPACING = 5;
	public static final int SPACING_FOR_DIGITS = 80;
	public static final int MARGIN_WIDTH = 3;
	public static final int MARGIN_HEIGHT = 15;

	private Composite tabComposite;
	private Student student;
	private MApplication appl;

	private Text textName;
	private CCombo comboGroup;
	private Text textAddress;
	private Text textCity;
	private Text textResult;

	private Button buttonSave;
	private Button buttonCancel;

	private static Logger log = Logger.getLogger(StudentTabItem.class.getName());

	public StudentTabItem(CTabFolder parent, int style, Composite tabComposite, Student student, MApplication appl) {
		super(parent, style);
		this.tabComposite = tabComposite;
		this.student = student;
		this.appl = appl;
		createTabItem(student);
	}

	private void createTabItem(Student student) {

		if (student != null) {
			tabComposite.setLayout(new FillLayout());
			this.setText(student.getName());
			this.student = student;
			this.setControl(tabComposite);

			initEditView(tabComposite);
			saveCancelDisactivate();
		} else {

			tabComposite.setLayout(new FillLayout());
			this.setText("New");
			this.setControl(tabComposite);

			initEditView(tabComposite);
			setComboGroupItems();
		}
	}

	private void initEditView(Composite tabComposite) {

		Composite inputComposite = new Composite(tabComposite, SWT.NONE);
		inputComposite.setLayout(new FillLayout(SWT.VERTICAL));

		Composite compositeName = new Composite(inputComposite, SWT.NONE);
		FillLayout flCompositeName = new FillLayout(SWT.HORIZONTAL);
		flCompositeName.spacing = SPACING;
		flCompositeName.marginWidth = MARGIN_WIDTH;
		flCompositeName.marginHeight = MARGIN_HEIGHT;
		compositeName.setLayout(flCompositeName);

		Label lblName = new Label(compositeName, SWT.NONE);
		lblName.setText("Name");

		textName = new Text(compositeName, SWT.BORDER);

		Composite compositeGroup = new Composite(inputComposite, SWT.NONE);
		FillLayout flCompositeGroup = new FillLayout(SWT.HORIZONTAL);
		flCompositeGroup.spacing = SPACING_FOR_DIGITS;
		flCompositeGroup.marginWidth = MARGIN_WIDTH;
		flCompositeGroup.marginHeight = MARGIN_HEIGHT;
		compositeGroup.setLayout(flCompositeGroup);

		Label lblGroup = new Label(compositeGroup, SWT.NONE);
		lblGroup.setText("Group");

		comboGroup = new CCombo(compositeGroup, SWT.RIGHT_TO_LEFT | SWT.BORDER);

		Composite compositeAddress = new Composite(inputComposite, SWT.NONE);
		FillLayout flCompositeAddress = new FillLayout(SWT.HORIZONTAL);
		flCompositeAddress.spacing = SPACING;
		flCompositeAddress.marginWidth = MARGIN_WIDTH;
		flCompositeAddress.marginHeight = MARGIN_HEIGHT;
		compositeAddress.setLayout(flCompositeAddress);

		Label lblAddress = new Label(compositeAddress, SWT.NONE);
		lblAddress.setText("Address");
		textAddress = new Text(compositeAddress, SWT.BORDER);

		Composite compositeCity = new Composite(inputComposite, SWT.NONE);
		FillLayout flCompositeCity = new FillLayout(SWT.HORIZONTAL);
		flCompositeCity.spacing = SPACING;
		flCompositeCity.marginWidth = MARGIN_WIDTH;
		flCompositeCity.marginHeight = MARGIN_HEIGHT;
		compositeCity.setLayout(flCompositeCity);

		Label lblCity = new Label(compositeCity, SWT.NONE);
		lblCity.setText("City");

		textCity = new Text(compositeCity, SWT.BORDER);

		Composite compositeResult = new Composite(inputComposite, SWT.NONE);
		FillLayout flCompositeResult = new FillLayout(SWT.HORIZONTAL);
		flCompositeResult.spacing = SPACING_FOR_DIGITS;
		flCompositeResult.marginWidth = MARGIN_WIDTH;
		flCompositeResult.marginHeight = MARGIN_HEIGHT;
		compositeResult.setLayout(flCompositeResult);

		Label lblResult = new Label(compositeResult, SWT.NONE);
		lblResult.setText("Result");

		textResult = new Text(compositeResult, SWT.RIGHT | SWT.BORDER);

		Composite compositeButtons = new Composite(inputComposite, SWT.NONE);
		FillLayout flCompositeButtons = new FillLayout(SWT.HORIZONTAL);
		flCompositeButtons.spacing = SPACING;
		flCompositeButtons.marginWidth = MARGIN_WIDTH;
		flCompositeButtons.marginHeight = MARGIN_HEIGHT;
		compositeButtons.setLayout(flCompositeButtons);

		buttonSave = new Button(compositeButtons, SWT.PUSH);

		buttonCancel = new Button(compositeButtons, SWT.PUSH);

		loadImageToButton(ACCEPT_ICON, buttonSave);
		loadImageToButton(CANCEL_ICON, buttonCancel);

		Composite photoComposite = new Composite(tabComposite, SWT.NONE);
		photoComposite.setLayout(new FormLayout());

		Canvas canvas = new Canvas(photoComposite, SWT.NONE);
		FormData fdCanvas = new FormData();
		fdCanvas.top = new FormAttachment(PERCENTS_FROM_TOP, getImageOffsetHeight(student));
		fdCanvas.left = new FormAttachment(PERCENTS_FROM_LEFT, getImageOffsetWidth(student));
		canvas.setLayoutData(fdCanvas);

		canvas.addPaintListener(new PaintListener() {

			@Override
			public void paintControl(PaintEvent e) {

				Image image = null;

				if (student != null) {

					if (student.getPhoto() == null) {
						image = loadImage(DEFAULT_PHOTO);

					} else {
						image = loadImage(student.getPhoto());
					}

					e.gc.drawImage(image, 0, 0);

					image.dispose();
				}
			}
		});

		buttonSave.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				IEclipseContext ctx = appl.getContext();
				GroupModel model = (GroupModelImpl) ctx.get(GroupView.MODEL);

				if (student == null) {

					if (verifyForm()) {

						MessageDialog dialog = new MessageDialog(getDisplay().getActiveShell(), "Saving", null,
								"Wanna save your changing?", MessageDialog.CONFIRM, new String[] { "Save", "Cancel" },
								0);
						int result = dialog.open();

						if (result == 0) {
							Student newStudent = addNewStudent(model);
							setText(newStudent.getName());
							student = newStudent;
							fillStudentData(newStudent);
							addFormListeners();
						}
					}

					setComboGroupItems();
				} else {

					if (verifyForm()) {

						MessageDialog dialog = new MessageDialog(getDisplay().getActiveShell(), "Saving", null,
								"Wanna save your changing?", MessageDialog.CONFIRM, new String[] { "Save", "Cancel" },
								0);
						int result = dialog.open();

						if (result == 0) {

							updateStudent(model);
							setComboGroupItems();
						}
					}
				}
			}
		});

		buttonCancel.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (student == null) {
					textName.setText("");
					comboGroup.setText("");
					textAddress.setText("");
					textCity.setText("");
					textResult.setText("");
				} else {

					fillStudentData(student);
				}
			}
		});

		if (student != null)
			fillStudentData(student);
		addFormListeners();

	}

	public void setComboGroupItems() {
		StudentTabFolder folder = (StudentTabFolder) this.getParent();
		folder.setComboGroupsItems();
	}

	private void loadImageToButton(String iconURI, Button button) {
		Image image = loadImage(iconURI);
		button.setImage(image);
	}

	private Image loadImage(String iconURI) {

		URL url = null;
		Image image = null;
		try {
			url = new URL(iconURI);
		} catch (MalformedURLException e) {
			log.info("Can't create URL");
			log.log(Level.SEVERE, "Exception:", e);
		}
		InputStream inStream = null;
		try {

			inStream = url.openConnection().getInputStream();
			image = new Image(getDisplay(), inStream);

		} catch (IOException e) {
			log.info("Can't open image file");
			log.log(Level.SEVERE, "Exception:", e);

		} finally {
			try {
				inStream.close();
			} catch (IOException e) {
				log.info("Can't close image file");
				log.log(Level.SEVERE, "Exception:", e);
			}
		}
		return image;
	}

	private Student addNewStudent(GroupModel model) {
		Student student = model.addStudent(textName.getText(), comboGroup.getText(), textAddress.getText(),
				textCity.getText(), Integer.valueOf(textResult.getText()), null);
		treeViewerRefresh();

		return student;
	}

	private void updateStudent(GroupModel model) {
		model.updateStudent(textName.getText(), comboGroup.getText(), textAddress.getText(), textCity.getText(),
				Integer.valueOf(textResult.getText()), null, student);
		treeViewerRefresh();
	}
	
	private void treeViewerRefresh() {
		TreeViewer treeViewer = (TreeViewer) appl.getContext().get(GroupView.TREE_VIEW);
		treeViewer.refresh();
	}

	private void fillStudentData(Student student) {

		textName.setText(student.getName());
		setComboGroupItems();
		comboGroup.setText(student.getGroup().getGroupName());
		textAddress.setText(student.getAddress());
		textCity.setText(student.getCity());
		textResult.setText(String.valueOf(student.getResult()));

		saveCancelDisactivate();
	}

	private void checkForChanges(ModifyEvent event) {

		if (student != null) {
			if (compareFormData()) {

				saveCancelDisactivate();
			} else {

				saveCancelActivate();
			}
		} else {

			if (textName.getText().equals("") || comboGroup.getText().equals("") || textAddress.getText().equals("")
					|| textCity.getText().equals("") || textResult.getText().equals("")) {

				saveCancelDisactivate();
			} else {
				saveCancelActivate();
			}
		}
	}
	
	private boolean compareFormData() {
			if (textName.getText().equals(student.getName())
					&& comboGroup.getText().equals(student.getGroup().getGroupName())
					&& textAddress.getText().equals(student.getAddress())
					&& textCity.getText().equals(student.getCity())
					&& Integer.parseInt(textResult.getText()) == student.getResult())
				return true; else
					return false;
	}

	public boolean verifyForm() {

		if (!InputVerificator.verifyName(textName.getText())) {
			saveCancelDisactivate();
			MessageDialog.openInformation(getDisplay().getActiveShell(), "Warning", "Wrong name");
		}

		if (!InputVerificator.verifyAddress(textAddress.getText())) {
			saveCancelDisactivate();
			MessageDialog.openInformation(getDisplay().getActiveShell(), "Warning", "Wrong address");
		}

		if (!InputVerificator.verifyCity(textCity.getText())) {
			saveCancelDisactivate();
			MessageDialog.openInformation(getDisplay().getActiveShell(), "Warning", "Wrong City");
		}

		if (InputVerificator.verifyName(textName.getText()) && InputVerificator.verifyAddress(textAddress.getText())
				&& InputVerificator.verifyCity(textCity.getText())) {
			return true;
		} else {
			return false;
		}
	}

	private void addFormListeners() {

		textName.addModifyListener(event -> checkForChanges(event));
		comboGroup.addModifyListener(event -> checkForChanges(event));
		textAddress.addModifyListener(event -> checkForChanges(event));
		textCity.addModifyListener(event -> checkForChanges(event));
		textResult.addModifyListener(event -> checkForChanges(event));
		comboGroup.addVerifyListener(event -> InputVerificator.verifyGroup(event));
		textResult.addVerifyListener(event -> InputVerificator.verifyResult(event));
	}

	private int getImageOffsetHeight(Student student) {
		Image image;
		if (student != null) {
			if (student.getPhoto() != null) {
				image = loadImage(student.getPhoto());
				return image.getBounds().height / 2;
			} else {
				image = loadImage(DEFAULT_PHOTO);
				return -image.getBounds().height / 2;
			}
		} else
			return 0;
	}

	private int getImageOffsetWidth(Student student) {
		Image image;
		if (student != null) {
			if (student.getPhoto() != null) {
				image = loadImage(student.getPhoto());
				return image.getBounds().width / 2;
			} else {
				image = loadImage(DEFAULT_PHOTO);
				return -image.getBounds().width / 2;
			}
		} else
			return 0;
	}
	
	private void saveCancelActivate() {
		buttonSave.setEnabled(true);
		buttonCancel.setEnabled(true);
	}

	private void saveCancelDisactivate() {
		buttonSave.setEnabled(false);
		buttonCancel.setEnabled(false);
	}

	public CCombo getComboGroup() {
		return comboGroup;
	}

	public Student getStudent() {
		return student;
	}
}
