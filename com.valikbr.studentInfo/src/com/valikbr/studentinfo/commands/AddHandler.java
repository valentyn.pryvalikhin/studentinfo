package com.valikbr.studentinfo.commands;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;

import com.valikbr.studentinfo.view.StudentTabFolder;
import com.valikbr.studentinfo.view.StudentView;

public class AddHandler {

	@Inject
	IEclipseContext ctx;

	@Inject
	MApplication appl;

	@Execute
	public void execute() {

		StudentTabFolder tabFolder = (StudentTabFolder) ctx.get(StudentView.TABFOLDER);

		tabFolder.createNewStdTab();
	}
}
