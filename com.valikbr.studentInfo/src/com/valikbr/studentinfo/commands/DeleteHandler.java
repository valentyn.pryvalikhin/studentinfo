package com.valikbr.studentinfo.commands;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Shell;

import com.valikbr.studentinfo.entities.Student;
import com.valikbr.studentinfo.model.GroupModel;
import com.valikbr.studentinfo.view.GroupView;
import com.valikbr.studentinfo.view.StudentTabFolder;
import com.valikbr.studentinfo.view.StudentView;

public class DeleteHandler {
	
	@Inject
	MApplication appl;
	
	@Execute
	public void execute(Shell shell) {

		Student studentForDel = (Student) appl.getContext().get(GroupView.SELECTED_STUDENT);
		GroupModel model = (GroupModel) appl.getContext().get(GroupView.MODEL);
		TreeViewer viewer = (TreeViewer) appl.getContext().get(GroupView.TREE_VIEW);
		StudentTabFolder tabFolder = (StudentTabFolder) appl.getContext().get(StudentView.TABFOLDER);
		
		MessageDialog dialog = new MessageDialog(shell, "Deleting", null,
				"Wanna delete student?", MessageDialog.CONFIRM, new String[] { "Delete", "Cancel" },
				0);
		int result = dialog.open();

		if (result == 0) {
			
			model.deleteStudent(studentForDel);
			tabFolder.setComboGroupsItems();
			tabFolder.closeTab(studentForDel);
			
			viewer.refresh();
		}
	}
}
