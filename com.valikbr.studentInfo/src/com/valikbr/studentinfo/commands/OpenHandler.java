package com.valikbr.studentinfo.commands;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import com.valikbr.studentinfo.IOOperations;
import com.valikbr.studentinfo.model.GroupModel;
import com.valikbr.studentinfo.view.GroupView;

public class OpenHandler {
	
	@Inject
	MApplication appl;

	@Execute
	public void execute(Shell shell){
		FileDialog fd = new FileDialog(shell, SWT.OPEN);
		fd.setText("Open");
		fd.setFilterPath("D:/");
		String[] filterExt = { "*.*", "*.txt", "*.json" };
		fd.setFilterExtensions(filterExt);
		String openedFileName = fd.open();
		TreeViewer viewer = (TreeViewer) appl.getContext().get(GroupView.TREE_VIEW);
		
		if (openedFileName != null) {
			
			List<GroupModel> groupModels = new ArrayList<>();
			GroupModel model = IOOperations.openJsonFile(openedFileName);
			groupModels.add(model);
			appl.getContext().set(GroupView.MODEL, model);
			viewer.setInput(groupModels);
			viewer.refresh();
		}
		

	}
}
