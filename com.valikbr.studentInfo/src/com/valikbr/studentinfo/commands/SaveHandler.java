package com.valikbr.studentinfo.commands;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import com.valikbr.studentinfo.IOOperations;
import com.valikbr.studentinfo.model.GroupModel;
import com.valikbr.studentinfo.view.GroupView;

public class SaveHandler {
	
	@Inject
	MApplication appl;

	@Execute
	public void execute(Shell shell){
		FileDialog fd = new FileDialog(shell, SWT.SAVE);
		fd.setText("Save");
		fd.setFilterPath("D:/");
		fd.setFileName("students.json");
		String[] filterExt = { "*.json", "*.txt", "*.*" };
		fd.setFilterExtensions(filterExt);
		String fileName = fd.open();

		if (fileName != null) {
			
		}
		GroupModel model = (GroupModel) appl.getContext().get(GroupView.MODEL);
		IOOperations.saveToJsonFile(fileName, model);
	}

}
