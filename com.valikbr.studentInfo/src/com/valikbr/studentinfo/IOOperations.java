package com.valikbr.studentinfo;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.valikbr.studentinfo.model.GroupModel;

public class IOOperations {
	
	public static GroupModel openJsonFile(String fileName) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			GroupModel model = mapper.readValue(new File(fileName), GroupModel.class);
			model.getLastStudentId();
			return model;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void saveToJsonFile(String fileName, GroupModel model) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(new File(fileName), model);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
