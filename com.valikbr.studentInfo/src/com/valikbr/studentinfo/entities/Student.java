package com.valikbr.studentinfo.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Student implements Serializable {

	private static final long serialVersionUID = 1418499354390037296L;
	private int id;
	private String name;
	private Group group;
	private String address;
	private String city;
	private int result;
	private String photo;

	public Student() {
	}

	public Student(int id, String name, Group group, String address, String city, int result, String photo) {
		this.id = id;
		this.name = name;
		this.group = group;
		this.address = address;
		this.city = city;
		this.result = result;
		this.photo = photo;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", group=" + group + ", address=" + address + ", city=" + city
				+ ", result=" + result + "]";
	}
}

