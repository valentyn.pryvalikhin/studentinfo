package com.valikbr.studentinfo.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "groupName")
public class Group implements Serializable {
	
	private static final long serialVersionUID = -4124420923160738953L;
	private String groupName;
//	@JsonManagedReference
	private Map<Integer, Student> students;
	
	

	public Group() {
	}

	public Group(String groupName) {
		this.groupName = groupName;
		students = new HashMap<Integer, Student>();
	}

	public String getGroupName() {
		return groupName;
	}

	public void addStudent(Student newStudent) {
		students.put(newStudent.getId() ,newStudent);
	}

	public Student getStudent(int studentId) {
		
		return students.get(studentId);
	}
	
	public boolean deleteStudent(Student student) {
		return students.remove(student.getId(), student);
	}

	public Map<Integer, Student> getStudents() {
		return students;
	}
}
