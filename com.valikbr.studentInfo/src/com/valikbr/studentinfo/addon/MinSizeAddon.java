package com.valikbr.studentinfo.addon;

import javax.annotation.PostConstruct;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class MinSizeAddon {
    @PostConstruct
    public void init(final IEventBroker eventBroker) {
        EventHandler handler = new EventHandler() {
            @Override
            public void handleEvent(Event event) {
                if (!UIEvents.isSET(event))
                    return;

                Object objElement = event.getProperty(UIEvents.EventTags.ELEMENT);
                if (!(objElement instanceof MWindow))
                    return;

                MWindow windowModel = (MWindow)objElement;
                Shell theShell = (Shell)windowModel.getWidget();
                if (theShell == null)
                    return;

                theShell.setMinimumSize(630, 440);
            }
        };
        eventBroker.subscribe(UIEvents.UIElement.TOPIC_WIDGET, handler);
    }
}