package com.valikbr.studentinfo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.valikbr.studentinfo.entities.Group;
import com.valikbr.studentinfo.entities.Student;

public class GroupModelImpl implements GroupModel, Serializable {

	private static final long serialVersionUID = 5526872420209061754L;
	private Map<String, Group> groups = new HashMap<String, Group>();
	private int nextId = 0;
	private AtomicInteger count = new AtomicInteger(nextId);

	public GroupModelImpl() {
	}

	@Override
	public Student addStudent(String name, String groupName, String address, String city, int result, String photo) {

		Group group = null;
		Student newStudent;
		int id = count.incrementAndGet();

		group = groups.get(groupName);

		if (group != null) {
			newStudent = new Student(id, name, group, address, city, result, photo);
			group.addStudent(newStudent);
			return newStudent;
		} else {
			group = new Group(groupName);
			newStudent = new Student(id, name, group, address, city, result, photo);
			groups.put(group.getGroupName(), group);
			group.addStudent(newStudent);
			return newStudent;
		}

	}

	@Override
	public Student getStudent(int studentId) {
		Student student = null;
		for (Map.Entry<String, Group> entry : groups.entrySet()) {
			student = entry.getValue().getStudent(studentId);
			if (student != null)
				return student;
		}
		return student;
	}

	@Override
	public Student updateStudent(String name, String groupName, String address, String city, int result, String photo,
			Student student) {
		if (groupName.equals(student.getGroup().getGroupName())) {

			fillStudentData(student, name, address, city, result, photo);
			return student;
		} else {
			Group toGroup = null;
			Group fromGroup = student.getGroup();

			fillStudentData(student, name, address, city, result, photo);

			if (groups.containsKey(groupName)) {
				toGroup = groups.get(groupName);
			}
			if (toGroup == null) {
				toGroup = new Group(groupName);
				groups.put(toGroup.getGroupName(), toGroup);
				student.setGroup(toGroup);
				moveStudent(student, fromGroup, toGroup);

				if (fromGroup.getStudents().isEmpty()) {
					groups.remove(fromGroup.getGroupName());
				}
				return student;
			} else {
				moveStudent(student, fromGroup, toGroup);
				student.setGroup(toGroup);

				if (fromGroup.getStudents().isEmpty()) {
					groups.remove(fromGroup.getGroupName());
				}
				return student;
			}
		}
	}

	@Override
	public boolean deleteStudent(Student student) {
		Group group = student.getGroup();
		boolean studentRemoved = group.deleteStudent(student);

		if (group.getStudents().isEmpty()) {
			groups.remove(group.getGroupName());
		}
		return studentRemoved;
	}

	@Override
	public Map<String, Group> getGroups() {
		Map<String, Group> copy = new HashMap<>(groups);
		return copy;
	}

	private void moveStudent(Student student, Group fromGroup, Group toGroup) {
		toGroup.addStudent(student);
		fromGroup.deleteStudent(student);
	}

	private void fillStudentData(Student student, String name, String address, String city, int result, String photo) {
		student.setName(name);
		student.setAddress(address);
		student.setCity(city);
		student.setResult(result);
		student.setPhoto(photo);
	}

	@Override
	public void getLastStudentId() {
		List<Student> studentList = new ArrayList();
		int maxId = 0;

		groups.values().forEach(g -> g.getStudents().values().forEach(s -> studentList.add(s)));

		for (Student stud : studentList) {
			if (stud.getId() > maxId)
				maxId = stud.getId();
		}
		nextId = maxId;
		count = new AtomicInteger(nextId);
	}
}
