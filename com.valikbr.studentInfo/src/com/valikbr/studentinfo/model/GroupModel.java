package com.valikbr.studentinfo.model;

import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.valikbr.studentinfo.entities.Group;
import com.valikbr.studentinfo.entities.Student;
@JsonDeserialize(as = GroupModelImpl.class)
public interface GroupModel {

	public Student addStudent(String name, String group, String address, String city, int result, String photo);

	public Student getStudent(int studentId);

	public Student updateStudent(String name, String group, String address, String city, int result, String photo, Student student);

	public boolean deleteStudent(Student student);

	public Map<String, Group> getGroups();
	
	public void getLastStudentId();
}
