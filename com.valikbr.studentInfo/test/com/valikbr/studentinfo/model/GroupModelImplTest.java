package com.valikbr.studentinfo.model;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.valikbr.studentinfo.entities.Student;

public class GroupModelImplTest {
	
	GroupModel groupModel;

	@Before
	public void setUp() throws Exception {
		groupModel = new GroupModelImpl();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddStudentToNewGroup() {
		//GIVEN
		//WHEN
		Student student = groupModel.addStudent("Vasyl", "1", "Lenona, 2", "Dnipro", 4, null);
		//THEN
		Assert.assertEquals(student, groupModel.getStudent(student.getId()));
		Assert.assertEquals("1", (groupModel.getGroups()).get("1").getGroupName());
		
	}
	
	@Test
	public void testAddStudentToExistingGroup() {
		//GIVEN
		groupModel.addStudent("Vasyl", "1", "Lenona, 2", "Dnipro", 4, null);
		//WHEN
		Student student = groupModel.addStudent("Petro", "1", "Kirova, 6", "Dnipro", 3, null);
		//THEN
		Assert.assertEquals(student, groupModel.getStudent(student.getId()));
		Assert.assertEquals("1", groupModel.getGroups().get("1").getGroupName());
	}

	@Test
	public void testGetStudent() {
		//GIVEN
		groupModel.addStudent("Vasyl", "1", "Lenona, 2", "Dnipro", 4, null);
		Student student = groupModel.addStudent("Petro", "1", "Kirova, 6", "Dnipro", 3, null);
		//WHEN
		Student gottenStudent = groupModel.getStudent(student.getId());
		//THEN
		Assert.assertEquals(student, gottenStudent);
	}
	
	@Test
	public void testUpdateStudentIfGroupNotUpdating() {
		//GIVEN
		groupModel.addStudent("Vasyl", "1", "Lenona, 2", "Dnipro", 4, null);
		Student student = groupModel.addStudent("Petro", "1", "Kirova, 6", "Dnipro", 3, null);
		//WHEN
		groupModel.updateStudent("Dmytro", "1", "Kirova, 6", "Dnipro", 3, null, student);
		//THEN
		Assert.assertEquals("Dmytro", groupModel.getStudent(student.getId()).getName());
	}
	
	@Test
	public void testUpdateStudentIfGroupUpdatingAndExist() {
		//GIVEN
		groupModel.addStudent("Vasyl", "1", "Lenona, 2", "Dnipro", 4, null);
		groupModel.addStudent("Semen", "2", "Lenona, 2", "Dnipro", 4, null);
		Student student = groupModel.addStudent("Petro", "1", "Kirova, 6", "Dnipro", 3, null);
		int studentId = student.getId();
		//WHEN
		groupModel.updateStudent("Dmytro", "2", "Kirova, 6", "Dnipro", 3, null, student);
		//THEN
		Assert.assertEquals("2", groupModel.getStudent(student.getId()).getGroup().getGroupName());
		Assert.assertEquals(student, groupModel.getGroups().get("2").getStudents().get(studentId));
		Assert.assertNull(groupModel.getGroups().get("1").getStudents().get(studentId));
	}
	
	@Test
	public void testUpdateStudentIfGroupUpdatingAndNotExist() {
		//GIVEN
		groupModel.addStudent("Vasyl", "1", "Lenona, 2", "Dnipro", 4, null);
		groupModel.addStudent("Semen", "2", "Lenona, 2", "Dnipro", 4, null);
		Student student = groupModel.addStudent("Petro", "1", "Kirova, 6", "Dnipro", 3, null);
		int studentId = student.getId();
		//WHEN
		groupModel.updateStudent("Dmytro", "3", "Kirova, 6", "Dnipro", 3, null, student);
		//THEN
		Assert.assertEquals("3", groupModel.getStudent(student.getId()).getGroup().getGroupName());
		Assert.assertEquals(student, groupModel.getGroups().get("3").getStudents().get(studentId));
		Assert.assertNull(groupModel.getGroups().get("1").getStudents().get(studentId));
	}

	@Test
	public void testDeleteStudentIfNotLastInGroup() {
		//GIVEN
		groupModel.addStudent("Semen", "1", "Lenona, 2", "Dnipro", 4, null);
		Student student = groupModel.addStudent("Petro", "1", "Kirova, 6", "Dnipro", 3, null);
		//WHEN
		boolean StudentDeleted = groupModel.deleteStudent(student);
		//THEN
		Assert.assertNull(groupModel.getStudent(student.getId()));
		Assert.assertTrue(StudentDeleted);
	}
	
	@Test
	public void testDeleteStudentIfLastInGroup() {
		//GIVEN
		groupModel.addStudent("Semen", "1", "Lenona, 2", "Dnipro", 4, null);
		Student student = groupModel.addStudent("Petro", "2", "Kirova, 6", "Dnipro", 3, null);
		int groupListLength = 1; 
		//WHEN
		boolean StudentDeleted = groupModel.deleteStudent(student);
		//THEN
		Assert.assertNull(groupModel.getStudent(student.getId()));
		Assert.assertEquals(groupListLength, groupModel.getGroups().size());
	}
}
